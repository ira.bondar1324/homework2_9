"use strict";

function outputArr(arr,parent = document.body){
    let list = document.createElement('ul');
    list.className = "list";
    parent.append(list);
    for (const elem of arr) {
        let item = document.createElement('li');
        item.textContent = elem;
        list.append(item);
    }
}

outputArr(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
  
  